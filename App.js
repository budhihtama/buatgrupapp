import React from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import AppStack from './src/navigator/AppStack';
import {Provider} from 'react-redux';
import store from './src/redux/store';

const App = () => {
  return (
      <Provider store={store}>
        <NavigationContainer>
          <AppStack />
        </NavigationContainer>
      </Provider>
  );
};

export default App;