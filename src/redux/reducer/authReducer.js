const initialState = {
    isLoading: false,
    isLoggedIn: false,
    data: [],
  };
  
  const auth = (state = initialState, action) => {
    switch (action.type) {
      case 'LOGIN': {
        return {
          ...state,
          isLoading: true,
        };
      }
      case 'LOGIN_SUCCESS': {
        return {
          ...state,
          isLoggedIn: true,
          isLoading: false,
        };
      }
      case 'LOGIN_FAILED': {
        return {
          ...state,
          isLoading: false,
        };
      }
      default:
        return state;
    }
  };
  
  export default auth;