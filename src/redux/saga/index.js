import {all} from 'redux-saga/effects';
import authSaga from './authSaga';
import grupSaga from './addGrupSaga';

export default function* rootSaga() {
  yield all([
    authSaga(),
    grupSaga()
  ]);
}