import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

export default function ProfileScreen() {
    return (
        <View style={styles.container}>
            <Text style={styles.txt}>Ini adalah halaman profile</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        borderColor: '#1D94A8',
        backgroundColor: '#1D94A8',
        borderWidth: 9,
        marginVertical: '60%',
        marginHorizontal: 20,
        borderRadius: 10
    },
    txt: {
        textAlign: 'center',
        fontSize: 30,
    }
})