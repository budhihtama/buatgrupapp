import React, {useState} from 'react';
import {View, Text, TouchableOpacity, TextInput} from 'react-native';
import {connect} from 'react-redux';

function LoginScreen(props){
	const [email, setEmail] = useState('tentor@gmail.com')
	const [password, setPassword] = useState('123123')


	const submit = () => {
      props.processLogin({email, password});
  };

	return (
		<View>
			<View>
				<Text style={{alignSelf:'center', marginVertical:20, fontSize: 20}}>Hi, Selamat Datang</Text>
				<TextInput  style={{alignSelf:'center'}}
		          value={email}
		          onChangeText={(text) => setEmail(text)}
		        />
		        <TextInput  style={{alignSelf:'center'}}
		          value={password}
		          onChangeText={(text) => setPassword(text)}
		          secureTextEntry
		        />

				<TouchableOpacity  style={{alignSelf:'center'}}
					onPress={() => submit()}
					>
					<Text>Login</Text>
				</TouchableOpacity>
			</View>
		</View>
		)
}


const mapStateToProps = (state) => ({
	
});

const mapDispatchToProps = (dispatch) => ({
	processLogin: (data) => dispatch({type: 'LOGIN', payload: data}),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);