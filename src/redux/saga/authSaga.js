import {takeLatest, put} from 'redux-saga/effects';
import {apiLogin} from './apiSaga';
import {
  removeToken,
  removeAccountId,
  saveAccountId,
  saveToken,
} from '../function/auth';
import {ToastAndroid} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

function* login(action) {
  try {
    const resLogin = yield apiLogin(action.payload);

    if (resLogin && resLogin.data) {
      yield saveToken(resLogin.data.data.token_access);

      yield put({type: 'LOGIN_SUCCESS'});
      ToastAndroid.showWithGravity(
      'Selamat Berhasil Login',
      ToastAndroid.SHORT,
      ToastAndroid.CENTER,
    );
      console.log('login sukses');
    } else {
      console.log('login gagal');
       ToastAndroid.showWithGravity(
      'Gagal Login',
      ToastAndroid.SHORT,
      ToastAndroid.CENTER,
    );
      yield put({type: 'LOGIN_FAILED'});
    }
  } catch (e) {
    console.log('login gagal', e);
    ToastAndroid.showWithGravity(
      'Gagal Login',
      ToastAndroid.SHORT,
      ToastAndroid.CENTER,
    );
    yield put({type: 'LOGIN_FAILED'});
  }
}

function* authSaga() {
  yield takeLatest('LOGIN', login);
}

export default authSaga;
