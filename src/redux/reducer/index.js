import {combineReducers} from 'redux';
import auth from './authReducer';
import grup from './grupReducer';

export default combineReducers({
  auth,
  grup
});
