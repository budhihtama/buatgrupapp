import React from 'react'
import { Text, View, StyleSheet } from 'react-native'

export default function LiveScreen() {
    return (
        <View style={styles.container}>
            <Text style={styles.txt}>Ini adalah Halaman Live</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        borderColor: '#1D94A8',
        backgroundColor: '#1D94A8',
        borderWidth: 9,
        marginVertical: '60%',
        marginHorizontal: 20,
        borderRadius: 10
    },
    txt: {
        textAlign: 'center',
        fontSize: 30,
    }
})
