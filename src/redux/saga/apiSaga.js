import axios from 'axios';

const baseUrl = 'http://bta70.omindtech.id/api';

export function apiLogin(dataLogin) {
  return axios({
    method: 'POST',
    url: baseUrl + '/tentor/login',
    data: dataLogin,
  });
}

export function apiCreateGroup(dataGroup,headers) {
  return axios({
    method: 'POST',
    url: baseUrl + '/grup',
    data: dataGroup,
    headers
  });
}