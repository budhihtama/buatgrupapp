const initialState = {
    isLoading: false,
    data: {
          kelas_id : null,
          nama : null,
          thumbnail : null
        },
  };
  
  const grup = (state = initialState, action) => {
    switch (action.type) {
      case 'ADD_GROUP': {
        return {
          ...state,
          isLoading: true,
        };
      }
      case 'ADD_GROUP_SUCCESS': {
        return {
          ...state,
          isLoading: false,
          data: action.payload
        };
      }
      case 'ADD_GROUP_FAILED': {
        return {
          ...state,
          isLoading: false,
        };
      }
      default:
        return state;
    }
  };
  
  export default grup;