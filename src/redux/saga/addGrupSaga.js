import {takeLatest, put} from 'redux-saga/effects';
import {apiCreateGroup} from './apiSaga';
import {getAccountId, getHeaders} from '../function/auth';
import {ToastAndroid} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

function* addGrup(action) {
  try {
    const headers = yield getHeaders();
    const accountId = yield getAccountId();

    const resGroup = yield apiCreateGroup(action.payload, headers);
    if (resGroup && resGroup.data) {
      console.log('ahoooy', resGroup.data);
    } else {
      console.log('tidak ada');
    }

    console.info('ini resGroup', resGroup.data);
    yield put({type: 'ADD_GROUP_SUCCESS', payload: resGroup.data});
    ToastAndroid.showWithGravity(
      'Selamat Berhasil Tambah Grup',
      ToastAndroid.SHORT,
      ToastAndroid.CENTER,
    );
    console.log('berhasil tambah grup');
  } catch (e) {
  	console.log('gagal', e)
    yield put({type: 'ADD_GROUP_FAILED'});
  }
}

function* grupSaga() {
  yield takeLatest('ADD_GROUP', addGrup);
}

export default grupSaga;