import React from 'react'
import { View, Text } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';

// Screens
import DiskusiScreen from '../screens/DiskusiScreen';

const Stack = createStackNavigator();

const DiskusiStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name='Diskusi' component={DiskusiScreen} />
        </Stack.Navigator>
    )
}

export default DiskusiStack
