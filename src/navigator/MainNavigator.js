import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { View, Text } from 'react-native'

// Screens
import HomeScreen from '../screens/HomeScreen';
import LiveScreen from '../screens/LiveScreen';
import DiskusiScreen from '../screens/DiskusiScreen';
import ProfileScreen from '../screens/ProfileScreen';




const Tab = createBottomTabNavigator();

export default function MainNavigator() {
    return (
        <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarLabel: ({ focused, color, size}) => {
              let textColor;

              if(route.name == 'Home') {
                textColor = focused ? '#1D94A8' : 'black'
              } else if(route.name == 'Live') {
                textColor = focused ? '#1D94A8' : 'black'
              } else if(route.name == 'Diskusi') {
                textColor = focused ? '#1D94A8' : 'black'
              } else if(route.name == 'Profile') {
                textColor = focused ? '#1D94A8' : 'black'
              }

              return (
                <Text style={{fontSize: 14, color: textColor, fontWeight: 'bold'}}>{route.name}</Text>
              )
            },
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;
  
              if (route.name === 'Home') {
                iconName = focused
                  ? 'home'
                  : 'home-outline';
              } else if (route.name === 'Live') {
                iconName = focused ? 'logo-youtube' : 'logo-youtube';
              } else if (route.name === 'Diskusi') {
                iconName = focused ? 'chatbubbles-outline' : 'chatbubbles';
              } else if (route.name === 'Profile') {
                iconName = focused ? 'person-outline' : 'person';
              }
              return <Ionicons name={iconName} size={35} color={color} />;
            },
          })}
          tabBarOptions={{
            activeTintColor: '#1D94A8',
            inactiveTintColor: 'black',
            style: {height: 70},
            tabStyle: {paddingVertical: 7}
          }}
        >
          <Tab.Screen name="Home" component={HomeScreen} />
          <Tab.Screen name="Live" component={LiveScreen} />
          <Tab.Screen name="Diskusi" component={DiskusiScreen} />
          <Tab.Screen name="Profile" component={ProfileScreen} />
        </Tab.Navigator>
    );
  }