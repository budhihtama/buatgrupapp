import React, {useState} from 'react'
import { Text, View, StyleSheet, TouchableOpacity, TextInput, Image, ImageBackground, Button, ToastAndroid} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import ImagePicker from 'react-native-image-picker'
import axios from 'axios';
import DropDownPicker from 'react-native-dropdown-picker';
import {useDispatch, connect} from 'react-redux'

const options = {
    title: 'Select Avatar',
    storageOptions: {
      privateDirectory: true,
      skipBackup: true,
      path: 'images',
    },
  };


function DiskusiScreen(props) {
    const [jenjang, setJenjang] = useState('jenjang')
    const [response, setResponse] = useState({});
    const [rawImage, setRawImage] = useState({});    
    const [kelasId, setkelasId] = useState(1)
    const [nama, setNama] = useState('')
    const [image, setImage] = useState()
    const dispatch = useDispatch()

    function SubmitData(){
      const dataPost =  new FormData()
      dataPost.append('kelas_id', jenjang)
      dataPost.append('nama', nama)
      dataPost.append('thumbnail', rawImage)

      axios('http://bta70.omindtech.id/api/grup', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MiwiZW1haWwiOiJ0ZW50b3JAZ21haWwuY29tIn0.QQ7dWCqm2T2XhMXz4734GSjS3kMlnwpxVoadCqKP28A'
        },
        data: dataPost
      })
      .then(response => {
        ToastAndroid.showWithGravity(
          'Selamat Berhasil Tambah Grup',
          ToastAndroid.SHORT,
          ToastAndroid.CENTER,
        );
        console.log(response)
      }).catch(err => {
        console.log(JSON.stringify(err))
      })
    }

    function pickImage() {
	    ImagePicker.showImagePicker(options, (response) => {
	      

	      if (response.didCancel) {
	        console.log('User cancelled image picker');
	      } else if (response.error) {
	        console.log('ImagePicker Error: ', response.error);
	      } else if (response.customButton) {
	        console.log('User tapped custom button: ', response.customButton);
	      } else {
	        const source = {
	          uri: response.uri,
	          type: response.type,
	          name: response.fileName,
	          data: response.data,
	        };

	        setRawImage(source);
	        setImage(response.uri);
	        console.log(response.uri)
	      }
	    });
	  }
    return (
        <View style={styles.container}> 
        {image == null ? (
          <TouchableOpacity
            onPress={() => pickImage()}
            >
              <View style={{width: 100, height: 100, backgroundColor: '#545454', alignSelf: 'center', alignItems: 'center', justifyContent: 'center'}}>
                <Icon name='camera' color='white' size={20}/>
              </View>
              <Text>
                Tambahkan foto group
              </Text>
            </TouchableOpacity>
        ) : (
            <Image source={{uri: image}} style={{width: 100, height: 100}} />
        )}
            

            <TextInput 
            value={nama}
            onChangeText={(text) => setNama(text)}
            style={{width: '80%', backgroundColor: '#545454', borderRadius: 10, marginVertical: 10}} />
            <DropDownPicker
                items={[
                    {label: '1', value: '1', hidden: true},
                    {label: '2', value: '2'},
                    {label: '3', value: '3'},
                ]}
                // defaultValue={jenjang}
                containerStyle={{height: 40, alignSelf: 'flex-start'}}
                style={{backgroundColor: '#545454', width: '50%' }}
                itemStyle={{
                    justifyContent: 'flex-start'
                }}
                dropDownStyle={{backgroundColor: '#fafafa'}}
                onChangeItem={item => setJenjang(item.value)}
            />

            <TouchableOpacity 
            onPress={() => SubmitData()}
            style={{width: '70%', height: 50, backgroundColor: 'blue', justifyContent: 'center', alignItems: 'center', marginVertical: 20}}>
              <Text style={{color: 'white'}}>
                Buat Grup
              </Text>
            </TouchableOpacity>
        </View>
    )
}

const mapStateToProps = (state) => ({
	
});

const mapDispatchToProps = (dispatch) => ({
	processLogin: (data) => dispatch({type: 'ADD_GROUP', payload: data}),
});

export default connect(mapStateToProps, mapDispatchToProps)(DiskusiScreen);


const styles = StyleSheet.create({
    container: {
        padding: 25,
        alignItems: 'center',
    },
    buttonUpload: {
        alignItems: 'center',
        marginTop: 50,
    },
    contInput: {
        flexDirection: 'column',
        marginTop: 30,
    },
    formInput: {
        backgroundColor: '#c3c7c4',
        borderRadius: 10,
        marginBottom: 15,
        paddingHorizontal: 20,
      },
    buttonGrup: {
        marginTop: '30%',
    },
    jenjang: {
        backgroundColor: '#c3c7c4',
        borderRadius: 10,
        marginBottom: 15,
        paddingLeft: 20,
        marginRight: '60%',
    },

})


