import React from 'react'
import { View, Text } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../screens/LoginScreen'
import MainNavigator from '../navigator/MainNavigator'
import {connect} from 'react-redux';
const Stack = createStackNavigator();

const AppStack = (props) => {
	return (
	
			<Stack.Navigator>
				{props.statusLogin ? (
					<Stack.Screen
						name="Main"
						component={MainNavigator}
						options={{headerShown: false}}
					/>
				) : (
					<Stack.Screen name="Login" component={Login} options={{headerShown: false}} />
				)}
			</Stack.Navigator>
		
	);
};

const mapStateToProps = (state) => ({
	statusLogin: state.auth.isLoggedIn,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(AppStack);